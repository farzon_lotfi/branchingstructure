int cellDimm = 2;
int cellDimmMax;
int COLS, ROWS;
int szDimm;
BranchingStructure bStruct;
Boolean bDebug = false, bPaused = false, bResize = false, bDrawGrid = false,
        bColor = false, bShowInstructions = false,
        bBoarderSeedsRnd = true, bDrawCircles = true, bDrawInCells = true, bDla;
final float stickinessArr[] = {.5,1,0.1,.01};
final float etaArr[] = {0,3,6};
int etaIndex = 0;
float currEta = etaArr[etaIndex];

final int walkFixed = 0, walk1AtAtime = 1, walkNAtAtime = 2, 
          walkNRec = 3, walkNCirc=4;
final String[] walkModesNames = { "walkFixed", "walk1AtAtime", "walkNAtAtime", 
                                  "walkNRec", "walkNCirc"};
final int drawRecBdr = 0, drawCircleBdr = 1;
int drawType = drawCircleBdr;
int walkType = walkNAtAtime;
int stickinessIndex = 1;
float stickiness = stickinessArr[stickinessIndex];
int currentMode = stickinessIndex;
int totalWalkers[] = {256, 512};
void setup() {
  szDimm = 600;
  size(600, 600);
  updateCellDimm();
}

void updateCellDimm() {
  cellDimmMax = szDimm/10;
  if(cellDimm > cellDimmMax) {
    cellDimm = cellDimmMax;
  }
  if(bDrawInCells) {
    COLS = width /cellDimm;
    ROWS = height/cellDimm;
  } else {
      COLS = width;
      ROWS = height;
  }
  bDla = (currentMode < 4);
  if(bDla) {
    bStruct = new DLA();
  } else {
    bStruct = new DBM();
  }
}

void drawGrid() {
  stroke(0,255,0);
  strokeWeight(2);
  for (int x=0; x<=COLS; x++) {
      line(x*cellDimm,0,x*cellDimm,height);
  }
  for (int y=0; y<=ROWS; y++) {
      line(0,y*cellDimm,width,y*cellDimm);
  }
}

void writeLine(String S, int i) {
    // writes S at line i
    text(S, 30, 25+i*10);
}

void drawInstructions() {
     if(bColor) {
       fill(color(255));
     } else {
       fill(color(255,0,0));
     }
     int L=0; // line counter, incremented below for ech line
     if(bShowInstructions) {
        writeLine("(press r) to enter resize " + (!bResize ? "window mode" : "cell dimmensions mode"),L++);
        writeLine("(press +) to increase " + (bResize ? "window size" : "cell dimmensions"),L++);
        writeLine("(press -) to decrease " + (bResize ? "window size" : "cell dimmensions"),L++);
        writeLine("cell dimmensions: " + cellDimm,L++);
        writeLine("window size: " + szDimm + " by " + szDimm,L++);
        writeLine("current mode: " + (currentMode < 4 ? "DLA " : "DBM ")+ currentMode,L++);
        writeLine("(press w) to toggle debug: " +((bDebug) ? "on": "off"), L++);
        writeLine("(press c) to toggle circle or rectangle", L++);
        if(bDla) {
          writeLine("(press 1) for DLA sticking factor of 1", L++);
          writeLine("(press 2) for DLA sticking factor of .1", L++);
          writeLine("(press 3) for DLA sticking factor of .01", L++);
          writeLine("(4-6 for DBM)",L++);
          writeLine("(press o) to cycle through random walk modes",L++);
          writeLine("current random walk: "+walkModesNames[walkType],L++);
          writeLine("(press d) toggles grid vs euclidean collison detection (dependent on c)"
           +((bDrawInCells) ? "on": "off"),L++);
          if(walkType < 3){ 
            writeLine("(press e) to toggle rand pos or boarder seeds",L++);
          }
        } else {
          writeLine("(1-3 for DLA)",L++);
          writeLine("(press 4) for DBM eta = 0", L++);
          writeLine("(press 5) for DBM eta = 3", L++);
          writeLine("(press 6) for DBM eta = 6", L++);
        }
        if(bDebug) {
          writeLine("(press g) toggle grid (large cell dimm debug tool)", L++);
        }
        writeLine("(press spacebar) Game is: " + (bPaused ? "paused" : "running") , L++);
     } else {
         writeLine("(press q) to show instructions.",L++);
     }
}


void draw() {
    background(0);
    bStruct.draw();
    drawInstructions();
    
    if(bDrawGrid) {
        drawGrid();
    }
}

void keyPressed() {
     switch(key) {
       // space bar - Start or stop the pattern growth (toggle between these).
       case ' ':
       bPaused = ! bPaused;
       break;
       // 1 - From a single seed, run DLA with a sticking factor of 1 (always sticks).
       case '1':
       stickinessIndex =1;
       stickiness = stickinessArr[stickinessIndex];
       currentMode = stickinessIndex;
       updateCellDimm();
       break;
       // 2 - From a single seed, run DLA with a sticking factor of 0.1.
       case '2':
       stickinessIndex =2;
       stickiness = stickinessArr[stickinessIndex];
       currentMode = stickinessIndex;
       updateCellDimm();
       break;
       // 3 - From a single seed, run DLA with a sticking factor of 0.01.
       case '3':
       stickinessIndex =3;
       stickiness = stickinessArr[stickinessIndex];
       currentMode = stickinessIndex;
       updateCellDimm();
       break;
       // 4 - From a single seed, run DBM with eta = 0.
       case '4':
       etaIndex = 0;
       currEta =  etaArr[etaIndex];
       currentMode = 4;
       updateCellDimm();
       break;
       // 5 - From a single seed, run DBM with eta = 3.
       case '5':
       etaIndex = 1;
       currEta =  etaArr[etaIndex];
       currentMode = 5;
       updateCellDimm();
       break;
       // 6 - From a single seed, run DBM with eta = 6.
       case '6':
       etaIndex = 2;
       currEta =  etaArr[etaIndex];
       currentMode = 6;
       updateCellDimm();
       break;
       // 7 - From a pattern of seeds, run DBM with eta = 3.
       case '7':
       etaIndex = 1;
       currEta =  etaArr[etaIndex];
       currentMode = 7;
       drawType = drawCircleBdr;
       updateCellDimm();
       break;
       // 8 - From a pattern of seeds, run DBM with eta = 0.
       case '8':
       etaIndex = 0;
       currEta =  etaArr[etaIndex];
       currentMode = 7;
       drawType = drawRecBdr;
       updateCellDimm();
       break;
       // 0 - Run DLA with a seed pattern and a sticking 
       // factor of your own choosing. Do not just use one seed cell!
       case '0':
       stickinessIndex =0;
       stickiness = stickinessArr[stickinessIndex];
       currentMode = 0;
       updateCellDimm();
       break;
       case 'q':
        bShowInstructions = ! bShowInstructions;
        break;
       case 'g':
        bDrawGrid = !bDrawGrid;
        break;
       case 'c':
       bDrawCircles = !bDrawCircles;
       break;
       case 'o':
       if(walkType < walkNCirc) {
            walkType++;
            if(walkType == walkNRec) {
              drawType = drawRecBdr;
            }
            if(walkType == walkNCirc) {
              drawType = drawCircleBdr;
            }
        } else {
          walkType = 0;
        }
       updateCellDimm();
       break;
       case 'w':
       bDebug = !bDebug;
       break;
       case 'e':
       bBoarderSeedsRnd = !bBoarderSeedsRnd;
       updateCellDimm();
       break;
       case 'd':
        bDrawInCells = !bDrawInCells;
        updateCellDimm();
        break;
       case 'r':
        bResize = !bResize;
        szDimm = width;
        break;
       case '+':
       case '=':
       bPaused = true;
       if(bResize) {
         if(szDimm <= 600) {
           szDimm +=100;
           surface.setSize(szDimm,szDimm);
         }
       }else {
        if(cellDimm < cellDimmMax) {
          cellDimm++;
        }
       }
       updateCellDimm();
       bPaused = false;
       break;
      case '-':
      bPaused = true;
      if(bResize) {
         if(szDimm > 100) {
           szDimm -=100;
           surface.setSize(szDimm,szDimm);
         }
       }else {
         if(cellDimm > 1) {
           cellDimm--;
        }
      }
      updateCellDimm();
      bPaused = false;
      break;
     }
   }