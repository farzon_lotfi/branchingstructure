import java.util.ListIterator;

interface BranchingStructure {
    void draw();
    boolean update(Seed seed);
}

class RandomWalker {
    ArrayList<Seed> walkSet = new ArrayList<Seed>();
    int numOfWalkers;
    RandomWalker() {
        initGrid();
        this.numOfWalkers = walkSet.size();
    }

    RandomWalker(int numOfWalkers) {
        this.numOfWalkers = numOfWalkers;
        for(int i = 0; i < numOfWalkers;i++) {
            walkSet.add(new Seed(color(255), bBoarderSeedsRnd));
        }
    }

    void initGrid() {
      int col4th = (COLS/4);
      int colhalf = (COLS/2);
      int rowhalf = (ROWS/2);
      int col34th = (3*COLS)/4;
      int row4th = (ROWS/4);
      int row34th = (3*ROWS)/4;
      switch(drawType) {
        case drawRecBdr:
          drawRecBdr(col4th, col34th, row4th, row34th);
        break;
        case drawCircleBdr:
          drawCirlceBdr(colhalf, rowhalf, row4th);
        break;
      }
    }

    void drawCirlceBdr(int i, int j, int radius) {
        int x = radius-1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);
         while (x >= y)
        {
            walkSet.add(new Seed(i + x, j + y,false));
            walkSet.add(new Seed(i + y, j + x,false));
            walkSet.add(new Seed(i - y, j + x,false));
            walkSet.add(new Seed(i - x, j + y,false));
            walkSet.add(new Seed(i - x, j - y,false));
            walkSet.add(new Seed(i - y, j - x,false));
            walkSet.add(new Seed(i + y, j - x,false));
            walkSet.add(new Seed(i + x, j - y,false));

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }

            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
    }

    void drawRecBdr(int sCol, int eCol, int sRow, int eRow) {
      for (int i = sCol; i < eCol; i++) {
           walkSet.add(new Seed(i,sRow,false));
           walkSet.add(new Seed(i,eRow,false));
         }
         for (int j = sRow; j < eRow; j++) {
           walkSet.add(new Seed(sCol,j,false));
           walkSet.add(new Seed(eCol,j,false));
         }
    }

    void walk(BranchingStructure bStruct) {
        ListIterator<Seed> iter = walkSet.listIterator(walkSet.size());
        // reverse iteration is faster removal for arraylists
        //System.out.println("start Loop");
        while(iter.hasPrevious()) {
            Seed currSeed = iter.previous();
            currSeed.randWalk();
            boolean seedAttached = bStruct.update(currSeed);
            if(seedAttached) {
                iter.remove();
            }
        }
        //System.out.println("end Loop");
    }

    void walkOneAtATime(BranchingStructure bStruct) {
        ListIterator<Seed> iter = walkSet.listIterator(walkSet.size());
        // reverse iteration is faster removal for arraylists
        while(iter.hasPrevious()) {
            Seed currSeed = iter.previous();
            currSeed.randWalk();
            boolean seedAttached = bStruct.update(currSeed);
            if(seedAttached) {
                iter.remove();
                walkSet.add(new Seed(color(255),bBoarderSeedsRnd));
            }
        }
    }

    void walkNAtATime(BranchingStructure bStruct) {
        if(walkSet.size() == 0) {
            for(int i = 0; i < numOfWalkers;i++) {
                walkSet.add(new Seed(color(255), bBoarderSeedsRnd));
            }
        }

        ListIterator<Seed> iter = walkSet.listIterator(walkSet.size());
        // reverse iteration is faster removal for arraylists
        while(iter.hasPrevious()) {
            Seed currSeed = iter.previous();
            currSeed.randWalk();
            boolean seedAttached = bStruct.update(currSeed);
            if(seedAttached) {
                iter.remove();
            }
        }
    }

    void draw() {
        for (int i = 0; i < walkSet.size(); i++) {
            walkSet.get(i).draw();
        }
    }
}

class DLA implements BranchingStructure {
    ArrayList<Seed> seeds = new ArrayList<Seed>();
    RandomWalker rWalker;
    // Initialize the grid with a single 
    // "seed" cell in the center of the grid. 
    DLA() {
        if(currentMode != 0) {
            seeds.add(new Seed(ROWS / 2, COLS / 2, true));
        } else {
            initGrid();
        }
        switch(walkType) {
            case walk1AtAtime:
            rWalker = new RandomWalker(1);
            break;
            case walkNAtAtime:
            rWalker = new RandomWalker(totalWalkers[0]);
            break;
            case walkNRec:
            case walkNCirc:
            rWalker = new RandomWalker();
            break;
            case walkFixed:
            rWalker = new RandomWalker(totalWalkers[1]);
            break;
        }
    }
    void draw() {
        for (int i = 0; i < seeds.size(); i++) {
            seeds.get(i).draw();
        }
        if(bDebug) {
            rWalker.draw();
        }
        if(bPaused) {
            return;
        }
        for(int i = 0; i <50;i++) {
            switch(walkType) {
            case walk1AtAtime:
                rWalker.walkOneAtATime(this);
            break;
            case walkNAtAtime:
            case walkNRec:
            case walkNCirc:
                rWalker.walkNAtATime(this);
            break;
            case walkFixed:
                rWalker.walk(this);
            break;
            }
        }
    }

    boolean update(Seed seed) {
        if (seed.isAttached(seeds)) {
            if( random(0,1) <= stickiness) {
                seeds.add(seed);
                seed.attach = true;
                seed.c=color(0,0,255);
                return true;
            }
        }
        return false;
    }
    void initGrid() {
        int col4th = (COLS/4);
        int colhalf = (COLS/2);
        int rowhalf = (ROWS/2);
        int col34th = (3*COLS)/4;
        int row4th = (ROWS/4);
        int row8th = (ROWS/8);
        int row34th = (3*ROWS)/4;
        drawRecBdr(colhalf, col34th, rowhalf, row34th);
        drawCirlceBdr(colhalf, rowhalf, row8th);
    }

    void drawCirlceBdr(int i, int j, int radius) {
        int x = radius-1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);
         while (x >= y)
        {
            seeds.add(new Seed(i + x, j + y,true));
            seeds.add(new Seed(i + y, j + x,true));
            seeds.add(new Seed(i - y, j + x,true));
            seeds.add(new Seed(i - x, j + y,true));
            seeds.add(new Seed(i - x, j - y,true));
            seeds.add(new Seed(i - y, j - x,true));
            seeds.add(new Seed(i + y, j - x,true));
            seeds.add(new Seed(i + x, j - y,true));

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }

            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
    }

    void drawRecBdr(int sCol, int eCol, int sRow, int eRow) {
      for (int i = sCol; i < eCol; i++) {
           seeds.add(new Seed(i,sRow,true));
           seeds.add(new Seed(i,eRow,true));
         }
         for (int j = sRow; j < eRow; j++) {
           seeds.add(new Seed(sCol,j,true));
           seeds.add(new Seed(eCol,j,true));
         }
    }

}
void drawShape(int x, int y,int diameter){
    if(bDrawCircles){
        int radius = diameter/2;
        ellipse(x+radius,y+radius,diameter,diameter);
    } else{
        rect(x,y,diameter,diameter);
    }
}

class Seed {
    int x,y;
    color c;
    boolean attach;
    Seed(int x, int y, boolean attach, color c) {
        this.x = x;
        this.y = y;
        this.c = c;
        this.attach = attach;
    }

    Seed(int x, int y, boolean attach) {
        this(x, y, attach, (attach? color(0,0,255) : color(255)));
    }
    void printSeed(){
        System.out.println("x: " +x +" y: " +y);
    }

    Seed(color c, boolean bBoarderSeeds) {
        this.attach = false;
        // New points are introduced at the borders and 
        // randomly (approximation of Brownian motion) 
        // walk until they are close enough to stick to an existing pixel.
        // http://paulbourke.net/fractals/dla/ (paragraph 3)
        if(bBoarderSeeds) {
            initBoardPnts();
        } else {
            // In general new points can be seeded 
            // anywhere in the image area, not just
            // around the border without any 
            // significant visual difference.
            this.x = (int)random(COLS);
            this.y = (int)random(ROWS);
        }
        this.c = c;
    }

    // see if a seed is touching a DLA tree
    boolean isAttached(ArrayList<Seed> tree) {
        for (int i = 0; i < tree.size(); i++) {
            Seed currTreeSeed = tree.get(i);
            if(bDrawInCells) {
                if(checkCellCollision(currTreeSeed)) {
                    return true;
                }
            } else {
                if(bDrawCircles) {
                    if(checkCollision(dist(currTreeSeed))) {
                        return true;
                    }
                } else {
                    if(boundingBoxCollision(currTreeSeed)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // Next, select a random position on the
    // grid at which to start, and start a 
    // random walk at that position. 
    void randWalk() {
        
        // left, right, or center
        this.x = constrain(this.x +getRandCellDirection() , 0, COLS);
        // up, down, or center
        this.y = constrain(this.y + getRandCellDirection(), 0, ROWS);
    }
    int getRandCellDirection(){
        int lowerBound = -1;
        int upperBound = 1;
        float vx = random(lowerBound, upperBound);
        if(vx >= .25) { // .75 range
            vx = ceil(vx);
        } if(vx < .25 && vx >= -.25) { // .5 range 
            vx = 0;
        } else { // less than -.25 // .75 range
            vx = floor(vx);
        }
        return (int) vx;
    }
    void initBoardPnts() {
        int i = floor(random(4));
        switch(i) {
            case 0:
                this.x = (int)random(COLS);
                this.y = 0;
            break;
            case 1:
                this.x = (int)random(COLS);
                this.y = ROWS;
            break;
            case 2:
                this.x = 0;
                this.y = (int) random(ROWS);
            break;
            case 3:
                this.x = COLS;
                this.y = (int) random(ROWS);
            break;
        }
    }

    int dist(Seed s2) {
        return (int) (pow(s2.x - this.x,2) + pow(s2.y - this.y,2));
    }

    float distSlow(Seed s2) {
        return sqrt(pow(2*s2.y-2*this.y,2)+ pow(2*s2.x-2*this.x,2));
    }

    // https://stackoverflow.com/a/1736741
    boolean checkCollision(int distance) {
        //return distance < 2*cellDimm;
        //So you can detect collision if:
        // (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
        // r1 = r2 = cellDimm/2
        //r1+r2 = cellDimm
        return (distance < pow(cellDimm,2));
    }

    boolean boundingBoxCollision(Seed s2) {
        return (this.x < s2.x + cellDimm &&
            this.x + cellDimm > s2.x &&
            this.y < s2.y + cellDimm &&
            this.y + cellDimm > s2.y);
    }

    boolean checkCellCollision(Seed s2) {
        return ((s2.x == this.x) && (s2.y == this.y+1))   || // top
        ((s2.x == this.x) && (s2.y == this.y - 1))   || // btm
        ((s2.x == this.x+1) && (s2.y == this.y))   || // right
        ((s2.x == this.x-1) && (s2.y == this.y))   || // left
        ((s2.x == this.x-1) && (s2.y == this.y-1)) || // btm-left
        ((s2.x == this.x+1) && (s2.y == this.y+1)) || // top-right
        ((s2.x == this.x-1) && (s2.y == this.y+1)) || // top-left
        ((s2.x == this.x+1) && (s2.y == this.y-1));  // btm-right
    }

    void draw() {
         noStroke();
         fill(c);
         if(bDrawInCells) {
             drawShape(this.x*cellDimm,this.y*cellDimm,cellDimm);
         } else {
             drawShape(this.x,this.y,cellDimm);
         }
    }
}