class PhiSeed extends Seed {
  static final int EMPTY=0, CANDIDATE=1, FILLED=2;
  int charge = EMPTY;
  float phi;
  float big_phi_eta;
  float pi;
  PhiSeed(int x, int y) {
    super(x,y,false,color(0));
    this.phi = 0;
    this.big_phi_eta = 0;
    this.pi = 0;
  }

  String stringify() {
    return "Pos {x:"+ x+ ", y:"+ y +
           " }, phi: "+phi+ ", big_phi_eta: "+ big_phi_eta + " pi: "+pi;
  }

  void setCharge(int value) {
    switch(value) {
    case EMPTY:
      this.attach = false;
      this.c = color(0);
      break;
    case FILLED:
      this.attach = true;
      this.c = color(0,0,255);
      break;
    case CANDIDATE: 
      this.attach = false;
      this.c = color(255,0,255);
      break;
    }
    this.charge = value;
  }
}

class DBM implements BranchingStructure {
    PhiSeed[][] grid;
    ArrayList<PhiSeed> seeds;
    ArrayList<PhiSeed> candidateSeeds;
    DBM() {
        grid = new PhiSeed[COLS][ROWS];
        seeds = new ArrayList<PhiSeed>();
        candidateSeeds = new ArrayList<PhiSeed>();
        for (int i = 0; i < COLS; i++) {
          for (int j = 0; j < ROWS; j++) { 
            grid[i][j] = new PhiSeed(i,j);
          }
        }
        if(currentMode == 7 ) {
          initGrid();
        } else {
          int colhalf = (COLS/2);
          int rowhalf = (ROWS/2);
          assert COLS == grid.length;
          assert ROWS == grid[0].length;
          grid[colhalf][rowhalf].setCharge(PhiSeed.FILLED);
          seeds.add(grid[colhalf][rowhalf]);
          laplace(colhalf,rowhalf);
          assert 1==seeds.size();
          assert 8 == candidateSeeds.size();
        }
    }

    void draw() {
      if(!bPaused) {
        update(seeds.get(seeds.size()-1));
      }
      if(bDebug) {
       for (int i = 0; i < COLS; i++) {
          for (int j = 0; j < ROWS; j++) { 
            grid[i][j].draw();
          }
       }
      } else {
        for(int i = 0; i < seeds.size(); i++ ) {
          seeds.get(i).draw();
        }
      }
    }

    boolean update(Seed seed) {
        if(seed instanceof PhiSeed) {
          if(candidateSeeds.size() == 0) {
            assert seeds.size() == COLS*ROWS;
            return false;
          }
          PhiSeed pSeed = (PhiSeed)seed;
          calcPhi(pSeed);
          int selectedSite = selectGrowthSite(currEta);
          addGrowthSite(selectedSite);
          return true;
        }
        return false;
    }
    // 3) add the growth site to a boundary condition
    void addGrowthSite(int selected) {
      PhiSeed  currSeed = candidateSeeds.get(selected);
      currSeed.setCharge(PhiSeed.FILLED);
      seeds.add(currSeed);
      candidateSeeds.remove(currSeed);
      laplace(currSeed.x, currSeed.y);
    }
    // 2) select a grdi cell as a growth site acording to phi
    int selectGrowthSite(float eta) {
        // calc min and max for eqn 13
        float phi_max = Float.MIN_VALUE;
        float phi_min = Float.MAX_VALUE;
        for(int i = 0; i < candidateSeeds.size(); i++ ) {
          PhiSeed  currSeed = candidateSeeds.get(i);
          float phi_i =  currSeed.phi;
          if(phi_i<phi_min) {
            phi_min=phi_i; 
          }
          if(phi_i >phi_max) {
            phi_max = phi_i;
          }
        }

        // eqn 13: big_phi_i = (phi_i - phi_min) / (phi_max - phi_min)
        float sum_phi_eta=0;
        for(int i = 0; i < candidateSeeds.size(); i++ ) {
          PhiSeed  currSeed = candidateSeeds.get(i);
          float phi_i =  currSeed.phi;
          // perform eqn 13
          float big_phi_i = (phi_i - phi_min)/(phi_max-phi_min);
          currSeed.big_phi_eta = pow(big_phi_i,eta);
          sum_phi_eta += currSeed.big_phi_eta;
        }

        float sum_pi=0;
        for(int i = 0; i < candidateSeeds.size(); i++) {
            // perform eqn 12 : pi = big_phi_i^η/ sigma[j=1,n] {big_phi_j^η}
            PhiSeed  currSeed = candidateSeeds.get(i);
            currSeed.pi = currSeed.big_phi_eta / sum_phi_eta;
            sum_pi+=currSeed.pi;
        }
        float[] partial_sums = new float[candidateSeeds.size()];
        partial_sums[0] =  candidateSeeds.get(0).pi;
        for(int i = 1; i < partial_sums.length; i++) {
          PhiSeed  currSeed = candidateSeeds.get(i);
          partial_sums[i] = currSeed.pi+ partial_sums[i-1];
        }
        assert sum_pi >= 0 && sum_pi <= 1.1;
        float r = random(sum_pi); //random(0, 1);
        int select = 0;
        for(int i = 0; i < partial_sums.length; i++) {
          if(r < partial_sums[i]) {
            select = i;
            break;
          }
        }
        return select;
    }

    // 1) calculate the electric potential phi on 
    // a regular grid acording to some boundary condition
    //phi = sigma[j=0,n] { 1- R1/rij } (eqn: 10)
    // ri,j is the distance between grid cell i and point charge j
    // n is the total number of point charges. (seeds.size())
    void calcPhi(PhiSeed pointCharge) {
      for(int i = 0; i < candidateSeeds.size(); i++ ) {
        PhiSeed  currSeed = candidateSeeds.get(i);
        float phi_i =  currSeed.phi;
        float dist = currSeed.distSlow(pointCharge);
        if(dist == 0) {
          System.out.println("currSeed:\n"+currSeed.stringify());
          System.out.println("pointCharge:\n"+pointCharge.stringify());
        }
        assert !Float.isNaN(dist) && dist > 0;
        phi_i = phi_i + 1 -(1.0/ dist);
        currSeed.phi = phi_i;
      }
    }

    // eqn 11: phi_i^t+1 = phi_i^t + (1- R1/ri,t+1)
    void updatePhi(PhiSeed nextTimeStepSeed) {
      float phi = 0;
      for(int i = 0; i < seeds.size(); i++ ) {
        PhiSeed  currSeed = seeds.get(i);
        float dist = currSeed.distSlow(nextTimeStepSeed);
        assert !Float.isNaN(dist) && dist > 0;
        phi = phi + (1 -(1.0/ dist));
      }
      nextTimeStepSeed.phi = phi;
    }

    int getIndex(int index, int modBy) {
      return (index + modBy) % modBy;
    }
    
    int getColIndex(int index) {
      return getIndex(index, COLS);
    }
    
    int getRowIndex(int index) {
      return getIndex(index, ROWS);
    }
    
    void initGrid() {
      int col4th = (COLS/4);
      int colhalf = (COLS/2);
      int rowhalf = (ROWS/2);
      int col34th = (3*COLS)/4;
      int row4th = (ROWS/4);
      int row34th = (3*ROWS)/4;
      switch(drawType) {
        case drawRecBdr:
          drawRecBdr(col4th, col34th, row4th, row34th);
        break;
        case drawCircleBdr:
          drawCirlceBdr(colhalf, rowhalf, row4th);
        break;
      }
    }

    void drawCirlceBdr(int i, int j, int radius) {
        int x = radius-1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);
         while (x >= y)
        {
            int ix = i + x;
            int iy = i + y;
            int jy = j + y;
            int jx = j + x;

            int inx = i - x;
            int iny = i - y;
            int jny = j - y;
            int jnx = j - x;

            grid[ix][jy].setCharge(PhiSeed.FILLED);
            grid[iy][jx].setCharge(PhiSeed.FILLED);
            grid[iny][jx].setCharge(PhiSeed.FILLED);
            grid[inx][jy].setCharge(PhiSeed.FILLED);
            grid[inx][jny].setCharge(PhiSeed.FILLED);
            grid[iny][jnx].setCharge(PhiSeed.FILLED);
            grid[iy][jnx].setCharge(PhiSeed.FILLED);
            grid[ix][jny].setCharge(PhiSeed.FILLED);
            seeds.add(grid[ix][jy]);
            seeds.add(grid[iy][jx]);
            seeds.add(grid[iny][jx]);
            seeds.add(grid[inx][jy]);
            seeds.add(grid[inx][jny]);
            seeds.add(grid[iny][jnx]);
            seeds.add(grid[iy][jnx]);
            seeds.add(grid[ix][jny]);
            
            candidateSeeds.remove(grid[ix][jy]);
            candidateSeeds.remove(grid[iy][jx]);
            candidateSeeds.remove(grid[iny][jx]);
            candidateSeeds.remove(grid[inx][jy]);
            candidateSeeds.remove(grid[inx][jny]);
            candidateSeeds.remove(grid[iny][jnx]);
            candidateSeeds.remove(grid[iy][jnx]);
            candidateSeeds.remove(grid[ix][jny]);

            laplace(ix,jy);
            laplace(iy ,jx);
            laplace(iny,jx);
            laplace(inx,jy);
            laplace(inx,jny);
            laplace(iny,jnx);
            laplace(iy ,jnx);
            laplace(ix ,jny);
            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }

            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
    }

    void drawRecBdr(int sCol, int eCol, int sRow, int eRow) {
      for (int i = sCol; i < eCol; i++) {
           grid[i][sRow].setCharge(PhiSeed.FILLED);
           grid[i][eRow].setCharge(PhiSeed.FILLED);
           laplace(i, sRow);
           laplace(i, eRow);
           seeds.add(grid[i][sRow]);
           seeds.add(grid[i][eRow]);
           candidateSeeds.remove(grid[i][sRow]);
           candidateSeeds.remove(grid[i][eRow]);
         }
         for (int j = sRow; j < eRow; j++) {
           grid[sCol][j].setCharge(PhiSeed.FILLED);
           grid[eCol][j].setCharge(PhiSeed.FILLED);
           laplace(sCol,j);
           laplace(eCol,j);
           seeds.add(grid[sCol][j]);
           seeds.add(grid[eCol][j]);
           candidateSeeds.remove(grid[sCol][j]);
           candidateSeeds.remove(grid[eCol][j]);
         }
    }
  
    void convolve(int x, int y) {
      if(grid[x][y].charge == PhiSeed.EMPTY) {
        grid[x][y].setCharge(PhiSeed.CANDIDATE);
        updatePhi(grid[x][y]);
        candidateSeeds.add(grid[x][y]);
      }
    }

    void laplace(int i, int j) {
        for(int x = i-1; x <= i+1; x++) {
          for(int y = j-1; y <= j+1;y++) {
            int xmod = getColIndex(x);
            int ymod = getRowIndex(y);
            convolve(xmod,ymod);
          }
        }
    }
}